import React, { useState } from 'react';
import Products from './components/Products/Products';
import Toolbar from './components/Toolbar/Toolbar';
import ShoppingCart from './components/ShoppingCart/ShoppingCart';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import orderData from './components/OrderData/OrderData';
import products from './data/products';

const allCategories = [
  'all',
  ...new Set(products.map((item) => item.category)),
];

const App = () => {
  const [cart, setCart] = useState(products);
  const [categories] = useState(allCategories);
  const [shopItems, setShopItems] = useState(products);

  const onAddItemHandler = (item) => {
    const index = cart.indexOf(item);
    const newCart = [...cart];
    newCart[index].qty++;
    setCart(newCart);
  };

  const onRemoveItemHandler = (item) => {
    const index = cart.indexOf(item);
    const newCart = [...cart];
    newCart[index].qty--;
    setCart(newCart);
  };

  const onFilterItemsHandler = (category) => {
    if (category === 'all') {
      setShopItems(products);
      return;
    }
    const newItems = products.filter((item) => item.category === category);
    setShopItems(newItems);
  };

  return (
    <>
      <Toolbar cartSize={cart.filter((item) => item.qty > 0).length} />
      <Switch>
        <Route
          path='/cart'
          render={() => (
            <ShoppingCart
              cart={cart.filter((item) => item.qty > 0)}
              onAddItem={onAddItemHandler}
              onRemoveItem={onRemoveItemHandler}
            />
          )}
        />
        <Route path='/order' component={orderData} />
        <Route
          path='/'
          exact
          render={() => (
            <Products
              productList={shopItems}
              onAddItem={onAddItemHandler}
              onRemoveItem={onRemoveItemHandler}
              categories={categories}
              filterItems={onFilterItemsHandler}
            />
          )}
        />
      </Switch>
    </>
  );
};

export default App;
