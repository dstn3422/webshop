import React from 'react';
import classes from './OrderData.module.css';

const orderData = () => {
  return (
    <div className={classes.OrderWrap}>
      <div className={classes.Order}>
        <form>
          <label htmlFor='name'>Name:</label>
          <input
            className={classes.Input}
            name='name'
            placeholder='Your name'
            type='text'
          />
          <label htmlFor='address'>Address:</label>
          <input
            className={classes.Input}
            name='address'
            placeholder='Your address'
            type='text'
          />
          <input className={classes.Submit} type='submit' />
        </form>
      </div>
    </div>
  );
};

export default orderData;
