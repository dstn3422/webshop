import React from 'react';
import classes from './ShoppingCart.module.css';
import { useHistory } from 'react-router-dom';
import nextId from 'react-id-generator';

const shoppingCart = ({ cart, onRemoveItem, onAddItem }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  let history = useHistory();

  const onOrderHandler = () => {
    history.push('/order');
  };

  return (
    <div className={classes.ShoppingCart}>
      {cart.map((item) => (
        <div className={classes.Item} key={nextId()}>
          <p>Item: {item.name}</p>
          <p>
            Price: {item.price} Qty: {item.qty}
          </p>
          <div className={classes.ProductControls}>
            <button
              onClick={() => onRemoveItem(item)}
              className={classes.RemoveItemButton}
            >
              Remove Item
            </button>
            <button
              onClick={() => onAddItem(item)}
              className={classes.AddItemButton}
            >
              Add item
            </button>
          </div>
        </div>
      ))}
      <p>
        Total Price: $
        {cart.reduce(
          (totalPrice, item) => totalPrice + item.price.substr(1) * item.qty,
          0
        )}
      </p>
      <button
        className={classes.OrderBtn}
        disabled={cart.length > 0 ? false : true}
        onClick={onOrderHandler}
      >
        Order
      </button>
    </div>
  );
};

export default shoppingCart;
