import React from 'react';
import { AddShoppingCart } from '@material-ui/icons';
import classes from './Product.module.css';

const product = ({ image, name, description, price, product, onAddItem }) => (
  <div className={classes.ProductItem}>
    <img src={image} alt={name} className={classes.ProductImage} />
    <h3 className={classes.ProductTitle}>{name}</h3>
    <p className={classes.ProductDescription}>{description}</p>
    <div className={classes.ProductPrice}>{price}</div>
    <button
      className={classes.AddShoppingCart}
      onClick={() => onAddItem(product)}
    >
      <AddShoppingCart />
    </button>
  </div>
);

export default product;
