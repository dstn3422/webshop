import React from 'react';
import Product from './Product/Product';
import classes from './Products.module.css';
import Categories from '../Catergories/Categories';

const products = ({
  productList,
  onAddItem,
  onRemoveItem,
  categories,
  filterItems,
}) => (
  <>
    <Categories categories={categories} filterItems={filterItems} />

    <main className={classes.Products}>
      {productList.map((product) => (
        <Product
          key={product.id}
          image={product.image}
          name={product.name}
          description={product.description}
          price={product.price}
          product={product}
          onRemoveItem={onRemoveItem}
          onAddItem={onAddItem}
        />
      ))}
    </main>
  </>
);

export default products;
