import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './Toolbar.module.css';
import { AddShoppingCart } from '@material-ui/icons';
import Badge from '../../UI/Badge/Badge';

const toolbar = ({ cartSize }) => {
  return (
    <div className={classes.Toolbar}>
      <NavLink to='/' className={classes.NavLink}>
        Home
      </NavLink>
      <NavLink to='/cart' className={classes.NavLink}>
        Shopping Cart
      </NavLink>
      <NavLink to='/cart' className={classes.Cart}>
        <AddShoppingCart className={classes.CartIcon} />
        <Badge num={cartSize} />
      </NavLink>
    </div>
  );
};

export default toolbar;
