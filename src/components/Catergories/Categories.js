import React from 'react';
import classes from './Categories.module.css';

const categories = ({ categories, filterItems }) => {
  return (
    <div className={classes.Categories}>
      {categories.map((category, index) => {
        return (
          <button
            type='button'
            className={classes.FilterButton}
            key={index}
            onClick={() => filterItems(category)}
          >
            {category}
          </button>
        );
      })}
    </div>
  );
};

export default categories;
