import React from 'react';
import classes from './Badge.module.css';

const badge = ({ num }) => <div className={classes.Badge}>{num}</div>;

export default badge;
