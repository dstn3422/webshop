const productList = [
  {
    id: 1,
    image:
      "https://images.puma.com/image/upload/f_auto,q_auto,b_rgb:fafafa,w_2000,h_2000/global/192790/02/sv01/fnd/EEA/fmt/png/Flyer-Runner-Engineered-Knit-Men's-Running-Shoes",
    name: 'Shoes',
    description: 'Running shoes.',
    price: '$25',
    category: 'Clothes',
    qty: 0,
  },
  {
    id: 2,
    image:
      'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/macbook-pro-13-og-202011?wid=600&hei=315&fmt=jpeg&qlt=95&.v=1604347427000',
    name: 'Macbook',
    description: '2 cool 4 you',
    price: '$2500',
    category: 'Tech',
    qty: 0,
  },
  {
    id: 3,
    image:
      'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/macbook-pro-13-og-202011?wid=600&hei=315&fmt=jpeg&qlt=95&.v=1604347427000',
    name: 'Macbook',
    description: '2 cool 4 you',
    price: '$2500',
    category: 'Tech',
    qty: 0,
  },
  {
    id: 4,
    image:
      'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/macbook-pro-13-og-202011?wid=600&hei=315&fmt=jpeg&qlt=95&.v=1604347427000',
    name: 'Macbook',
    description: '2 cool 4 you',
    price: '$2500',
    category: 'Tech',
    qty: 0,
  },
];

export default productList;
